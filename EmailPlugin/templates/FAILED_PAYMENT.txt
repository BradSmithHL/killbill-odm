The most recent subscription payment to {{text.merchantName}} for Order Display Monitor was declined by the bank.

Here are the details of the declined payment:

{{text.invoiceNumber}}: {{invoice.invoiceNumber}}
{{text.paymentDate}}: {{invoice.invoiceDate}}

{{#invoice.invoiceItems}}
{{startDate}} {{planName}} : {{formattedAmount}}
{{/invoice.invoiceItems}}

{{text.invoiceAmountPaid}}:  {{invoice.formattedPaidAmount}}
{{text.invoiceAmountTotal}}: {{invoice.formattedBalance}}


Sysnet ID:{{account.externalKey}}
{{text.billedTo}}
{{account.name}}
{{account.address1}}
{{account.city}}, {{account.stateOrProvince}} {{account.postalCode}}
{{account.country}}


To ensure continued access to {{text.productName}}, please update your billing information within 14 days of your last statement.

If you have any questions about your account, please contact {{text.merchantName}} via email: {{text.merchantContactEmail}} or phone: {{text.merchantContactPhone}}
