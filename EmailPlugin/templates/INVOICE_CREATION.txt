*** New Invoice ***

There is a new invoice from {{text.merchantName}}, due on {{invoice.targetDate}}.

{{#invoice.invoiceItems}}
{{startDate}} {{planName}} : {{formattedAmount}}
{{/invoice.invoiceItems}}

{{text.invoiceAmountTotal}}: {{invoice.formattedBalance}}

Sysnet ID:{{account.externalKey}}
{{text.billedTo}}
{{account.name}}
{{account.address1}}
{{account.city}}, {{account.stateOrProvince}} {{account.postalCode}}
{{account.country}}

If you have any questions about your account, please contact {{text.merchantName}} via email: {{text.merchantContactEmail}} or phone: {{text.merchantContactPhone}}

