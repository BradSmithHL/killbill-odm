This section describes how to install and configure and run the email-notifications plugin for killbill for ODM.
The plugin code can be found at:
https://github.com/killbill/killbill-email-notifications-plugin


To install the email-notifications plugin

- login to kaui
https://killbill.hlcloud.com.au/users/sign_in
u: hladmin
p: Dav3Luvskayn3

Choose tenant: H&L Australia ODM

navigate to : https://killbill.hlcloud.com.au/kpm/plugins

Click the cloud download button next to  email-notifications 


Setup the plugin with the following curl command:

==========
curl -v \
-X POST \
-u hladmin:Dav3Luvskayn3 \
-H 'X-Killbill-ApiKey: &|)zsF%g0)rCPO|ls);O' \
-H 'X-Killbill-ApiSecret: EO:bQRz1`]hq+bv|p2DB' \
-H 'X-Killbill-CreatedBy: hladmin' \
-H 'Content-Type: text/plain' \
-d 'org.killbill.billing.plugin.email-notifications.defaultEvents=INVOICE_NOTIFICATION,INVOICE_CREATION,INVOICE_PAYMENT_SUCCESS,INVOICE_PAYMENT_FAILED,SUBSCRIPTION_CANCEL
org.killbill.billing.plugin.email-notifications.smtp.host=smtp.gmail.com
org.killbill.billing.plugin.email-notifications.smtp.port=465
org.killbill.billing.plugin.email-notifications.smtp.useAuthentication=true
org.killbill.billing.plugin.email-notifications.smtp.useSSL=true
org.killbill.billing.plugin.email-notifications.smtp.userName=hlcloud.noreply@gmail.com
org.killbill.billing.plugin.email-notifications.smtp.password=WFmc@20#
org.killbill.billing.plugin.email-notifications.smtp.defaultSender=hlcloud.noreply@gmail.com' \
https://killbill.hlcloud.com.au:8080/1.0/kb/tenants/uploadPluginConfig/killbill-email-notifications
==========

** You may need to login to the killbill sever and delete existing templates from the database. see ../README.txt

The mysql command that you will need is:
Delete from tenant_kvs where tenant_key like 'killbill-email-notifications:%';

On the command line run (from this projects root dir): node ./EmailPlugin/upload_templates.json

All the templates are stored in EmailPlugin/templates.



