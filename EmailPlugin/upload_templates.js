const request = require("request");

const fs = require('fs');


const filePath = "./EmailPlugin/templates/";

let templateFiles = fs.readdirSync(filePath);


templateFiles = templateFiles.filter( name => name.substring(name.length-3) === "txt" );

const headers = {
    "Content-Type" : "text/plain",
    'X-Killbill-ApiKey' : '&|)zsF%g0)rCPO|ls);O',
    'X-Killbill-ApiSecret': 'EO:bQRz1`]hq+bv|p2DB',
    'X-Killbill-CreatedBy': 'admin'
};

const auth = {
    user: "hladmin",
    pass: "Dav3Luvskayn3"
}

const urlBase = "https://killbill.hlcloud.com.au:8080/1.0/kb/tenants/userKeyValue/killbill-email-notifications:";

const regionLang = "_en_US";

templateFiles.forEach(templateFileName => {
    
    const templateName = templateFileName.substring(0, templateFileName.length - 4);
    console.log(templateName);

    const currentURL = urlBase + templateName + regionLang;
    console.log("deleting existing template: " + templateName)
    request.delete(
        currentURL,
        {
            headers,
            auth,
        },
        (err, response) => {
            if (err) {
                console.log("An error ocurred while deleting " + templateName + " err: " + err);
            }
            else {
                console.log("deleted " + templateName + " with http status code: " + response.statusCode);
            }
            console.log("uploading new template: " + templateName)
            fs.createReadStream(filePath  + templateFileName).pipe(
                request.post(
                    currentURL, 
                    {
                        headers,
                        auth,
                    },
                    (err, response, body) => {
                        if (err) {
                            console.log("An error ocurred while uploading " + templateName + " err: " + err);
                        }
                        else {
                            console.log("uploaded " + templateName + " with http status code: " + response.statusCode);
                        }
                    }
                )
            );
        
        }
    );


});



