This document has  sections

- How to Setup 'stripe-plugin' from Scratch
- How to configure a customer to use the stripe plugin


How to configure a customer to use the stripe plugin
=====================

node stripe-get-token

Post the token to your customer (substitute the <<customerID>> with the appropriate killbill customerID )
(substritute the <<tokenID>> with the tokenid got from the previous call)

===========
curl -v \
-X POST \
-u hladmin:Dav3Luvskayn3 \
-H "Content-Type: application/json" \
-H 'X-Killbill-ApiKey: &|)zsF%g0)rCPO|ls);O' \
-H 'X-Killbill-ApiSecret: EO:bQRz1`]hq+bv|p2DB' \
-H 'X-Killbill-CreatedBy: admin' \
-H "X-Killbill-Reason: test card" \
-H "X-Killbill-Comment: test card" \
-d '{ 
    "pluginName": "killbill-stripe",
    "pluginInfo": {
        "properties": [
        {
            "key": "token",
            "value": "<<tokenID>>"
        }
        ] 
    }   
}' \
"https://killbill.hlcloud.com.au:8080/1.0/kb/accounts/<<customerID>>/paymentMethods?isDefault=true" 
===========





===========
===========
===========
===========
===========

How to Setup 'stripe-plugin' from Scratch
=====================

** Currently has a problem with customers with whose timezone is not 'Etc/UTC'
** This may be due to the Ruby KillBill plugin module or the stripe pluging module - not sure yet.


Download the latest Stripe plugin from : 

https://search.maven.org/search?q=g:org.kill-bill.billing.plugin.ruby

we need: stripe-plugin  v6.0.1  tar.gz


- login to kaui
https://killbill.hlcloud.com.au/users/sign_in
u: hladmin
p: Dav3Luvskayn3

Choose tenant: H&L Australia ODM

navigate to : https://killbill.hlcloud.com.au/kpm/
uninstall the exisint plugin (for a new killbill setup)


navigate to : https://killbill.hlcloud.com.au/kpm/plugins

Upload the latest plugin at the bottom of the from
name: stripe
version: 6.0.1
type: ruby

Navigate to:  https://killbill.hlcloud.com.au/kpm/

Check the status of the plugin - click the 'reload' or 'play' button if necessary next to the 'stripe-plugin'

You will need a stripe account

Configure the plugin using curl ( substitute api_secret_key && api_publishable_key details with appropriate details)

===========
curl -v \
-X POST \
-u hladmin:Dav3Luvskayn3 \
-H "Content-Type: text/plain" \
-H 'X-Killbill-ApiKey: &|)zsF%g0)rCPO|ls);O' \
-H 'X-Killbill-ApiSecret: EO:bQRz1`]hq+bv|p2DB' \
-H 'X-Killbill-CreatedBy: admin' \
-d ':stripe:
  :api_secret_key: "sk_test_jRcRRsT303YBXzVnGZ09XI2M"
  :api_publishable_key: "pk_test_XiPhyJki43YxwSBuss72sh7j"
  :fees_amount: "0"
  :fees_percent: "0"' \
https://killbill.hlcloud.com.au:8080/1.0/kb/tenants/uploadPluginConfig/killbill-stripe
===========


(make sure a phone number is verified on your stripe account) https://dashboard.stripe.com/phone-verification
Add a credit card to your stripe account and get the token:


