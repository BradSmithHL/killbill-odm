(function(exports){

    var BASE_RESOURCE, _get, _post;

    if (typeof require !== 'undefined') {
        var killbill = require('../killbill');
        BASE_RESOURCE = killbill.BASE_RESOURCE;
        _get = killbill._get;
        _post = killbill._post;
        Account = require('./Account')
    }
    else {
        BASE_RESOURCE = exports.BASE_RESOURCE;
        _get = exports._get;
        _post = exports._post;
    }

    var Invoice = {
        INVOICE_RESOURCE: BASE_RESOURCE + '/invoices',

        getById: function(invoiceId, params, callback) {
            if (typeof params === 'function') {
                callback = params;
                params = {};
            }
            _get(Invoice.INVOICE_RESOURCE + '/' + invoiceId, params, {}, callback);
        },

        getByAccountId: function(accountId, params, callback) {
            if (typeof params === 'function') {
                callback = params;
                params = {};
            }
            _get(Account.ACCOUNT_RESOURCE + '/' + accountId + '/invoices', params, {}, callback);
        },
        triggerInvoices: function(accountId, targetDate, user, reason, comment, callback) {
            console.log
            params = {
                accountId, 
                targetDate
            };
            var options = {user, reason, comment};

            _post(Invoice.INVOICE_RESOURCE, {}, params, options, callback);
        }
    };

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = Invoice;
    }
    else {
        exports.Invoice = Invoice;
    }

})(typeof exports === 'undefined'? this['K'] : exports);