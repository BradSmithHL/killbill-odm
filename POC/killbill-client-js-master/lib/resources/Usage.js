(function(exports){
    var BASE_RESOURCE, _post;

    if (typeof require !== 'undefined') {
        var killbill = require('../killbill');
        BASE_RESOURCE = killbill.BASE_RESOURCE;
        _post = killbill._post;
        _get = killbill._get;
        _put = killbill._put;
        _delete = killbill._delete;
        _merge = killbill._merge;

    }
    else {
        BASE_RESOURCE = exports.BASE_RESOURCE;
        _post = exports._post;
        _get = killbill._get;
        _put = killbill._put;
        _delete = killbill._delete;
        _merge = killbill._merge;
    }

    var usage = {
        USAGE_RESOURCE: BASE_RESOURCE + '/usages',

        create: function (usageRecord, user, reason, comment, params, callback) {
            if (typeof params === 'function') {
                callback = params;
                params = {};
            }
            var options = {user: user, reason: reason, comment: comment};
            _post(usage.USAGE_RESOURCE, usageRecord, params, options, callback);
        },

        getByDate: function(subscriptionId, startDate, endDate, params, callback) {
            if (typeof params === 'function') {
                callback = params;
                params = {};
            }
            params = _merge(params, {startDate, endDate});
            _get(usage.USAGE_RESOURCE + '/' + subscriptionId, params, {}, callback);
    
        },
    };



    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = usage;
    }
    else {
        exports.Usage = usage;
    }

})(typeof exports === 'undefined'? this['K'] : exports);