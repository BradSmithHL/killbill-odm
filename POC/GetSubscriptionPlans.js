// example of ho9w to get plans from kilbill api

const rp = require('request-promise');

const baseUrl = 'https://killbill.hlcloud.com.au:8080';
const user = "hladmin";
const pass = "Dav3Luvskayn3";
const apiKey = '&|)zsF%g0)rCPO|ls);O';
const apiSecret = 'EO:bQRz1`]hq+bv|p2DB';


let path = '/1.0/kb/catalog';

const method = "GET";

const options = {
    method: method,
    uri: baseUrl + path,
    auth: {
        user,
        pass,
        sendImmediately: false
    },
    headers: {
        'User-Agent': 'Request-Promise',
        'Content-Type': 'application/json',
        'X-Killbill-ApiKey': apiKey,
        'X-Killbill-ApiSecret': apiSecret,
        'Accept': 'application/json'
    },
    json: true, // Automatically parses the JSON string in the response
    resolveWithFullResponse: true
};

rp(options).then((res) => {
    console.log(res.statusCode);

    res.body[0].products.forEach( product => {
        console.log("'" + product.name + "' plans: "); 
        product.plans.forEach( plan => {
            console.log("\t", plan.name); 
        });
    });
});


// curl -v -u 'hladmin:Dav3Luvskayn3' -H 'X-Killbill-ApiKey: &|)zsF%g0)rCPO|ls);O'  -H 'X-Killbill-ApiSecret: EO:bQRz1`]hq+bv|p2DB' -H 'Accept: application/json'  'https://killbill.hlcloud.com.au:8080/1.0/kb/catalog/availableBasePlans' 