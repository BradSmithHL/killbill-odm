This section contains initial 'Proof of Concept' code, which helped determine whether killbill was appropriate for our needs.

It uses:
killbill-client-js-master 
https://github.com/killbill/killbill-client-js/blob/master/README.md
We should probably contribute to this project. It is currently used in index.js

With some modifications 

There are a number of functions that are useful:

/**
 * This function triggers an invoice run for a customer for a particular date. Useful for testing purposes 
 * because KillBill doesnt always trigger invoices runs straight away.
 * @param {*} externalKey sysnetid for the account you want to trigger.
 * @param {*} invoiceDateEpoch The epoch timestamp for the date you want to trigger invoices for.
 */
function triggerInvoices(externalKey, invoiceDateEpoch)

/**
 * This function gets invoices for a customer.
 * @param {*} externalKey - a string containing the sysnetkey for the customer you want to get unpaid invoices for
 */
function getInvoices(externalKey) {


/**
 * Adds Usage information for an account, for given a subscription plan name for a given usage type on a given date
 * @param {*} externalKey - The sysnet id for the account you want to update
 * @param {*} subscriptionPlanName  - The name of the plan you want to add usage for.
 * @param {*} unitType  - the type of usage you want to add 
 * @param {*} units - integer containing the quantity of usage
 * @param {*} usageDateEpoch epoch timestamp for the date you want to add usage information for
 */
function addUsageToAccountSubscription(externalKey, subscriptionPlanName, unitType, units, usageDateEpoch)


To use:

Add appropriate calls to index.js and from this directory:

> node ./index.js

