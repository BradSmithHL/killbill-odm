const killbill = require('./killbill-client-js-master/lib/killbill');
const AccountAPI = require('./killbill-client-js-master/lib/resources/Account');
const UsageAPI = require('./killbill-client-js-master/lib/resources/Usage');
const InvoiceAPI = require('./killbill-client-js-master/lib/resources/Invoice');

killbill.settings.url = 'https://killbill.hlcloud.com.au:8080';
killbill.settings.username = "hladmin";
killbill.settings.password = "Dav3Luvskayn3";
killbill.settings.apiKey = '&|)zsF%g0)rCPO|ls);O';
killbill.settings.apiSecret = 'EO:bQRz1`]hq+bv|p2DB';

const dayInMillis = (24 * 60 * 60 * 1000);








//const usageDateEpoch = Date.now() + (2 * dayInMillis);

 //addUsageToAccountSubscription("sysnetkey2", "postpaid-odm-daily",  "odm-devices-daily", 2, usageDateEpoch );

// addUsageToAccountSubscription("billcycleday18", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch);


// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch);
// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch - (1 * dayInMillis));
// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch - (2 * dayInMillis));
// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch - (3 * dayInMillis));
// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch - (4 * dayInMillis));
// addUsageToAccountSubscription("sysnetkey3", "postpaid-odm-daily", "odm-devices-daily", 2, usageDateEpoch - (5 * dayInMillis));

const usageDateEpoch = Date.now() - (2 * dayInMillis);




//addUsageToAccountSubscription("samt6test", "postpaid-odm-daily", "odm-devices-daily", 4, usageDateEpoch);

//triggerInvoices("sysnetkey7 ", usageDateEpoch + dayInMillis);



getInvoices("billcycleday18", Date.now() - (2* dayInMillis));
//addUsageToAccountSubscription("billcycleday18", "postpaid-odm-daily", "odm-devices-daily", 8, usageDateEpoch);


//getUsages("sysnetkey4", "postpaid-odm-daily", Date.now() - (2 * dayInMillis), Date.now())

/**
 * This function gets invoices for a customer.
 * @param {*} externalKey - a string containing the sysnetkey for the customer you want to get unpaid invoices for
 */
function getInvoices(externalKey) {
    
    AccountAPI.getByExternalKey(externalKey, "", (errAccountAPI, account) => {
        if (errAccountAPI) console.log("Account.get Err: ", errAccountAPI);
        console.log("AccountAPI.getByExternalKey :\n", account);

        InvoiceAPI.getByAccountId(account.accountId, { withItems: true, }, (err, invoices) => {
            console.log(err);
            console.log("InvoiceAPI.getByAccountId", JSON.stringify(invoices, null, 2));
        });

    });
    
}

/**
 * This function triggers an invoice run for a customer for a particular date. Useful for testing purposes 
 * because KillBill doesnt always trigger invoices runs straight away.
 * @param {*} externalKey sysnetid for the account you want to trigger.
 * @param {*} invoiceDateEpoch The epoch timestamp for the date you want to trigger invoices for.
 */
function triggerInvoices(externalKey, invoiceDateEpoch) {

    AccountAPI.getByExternalKey(externalKey, "", (errAccountAPI, account) => {
        if (errAccountAPI) console.log("Account.get Err: ", errAccountAPI);
        console.log("AccountAPI.getByExternalKey :\n", account);

        const invoiceDate = formatDateYYMMDD(invoiceDateEpoch);
        InvoiceAPI.triggerInvoices(account.accountId, invoiceDate, "samtest", "for fun bro", "Usage goes here!!", (invoiceErr, response) => {
            if (invoiceErr) console.log("triggerInvoice Error: ", invoiceErr)

            console.log("InvoiceAPI.triggerInvoices", response);
            
            AccountAPI.getInvoices(account.accountId, invoiceDate, true, true, undefined, (err, invoices) => {
                if (err) console.log("AccountAPI.getInvoices ERR: ", err);
                console.log("AccountAPI.getInvoices", JSON.stringify(invoices, null, 2));
            })
            
        });
    });
}


/**
 * Adds Usage information for an account, for given a subscription plan name for a given usage type on a given date
 * @param {*} externalKey - The sysnet id for the account you want to update
 * @param {*} subscriptionPlanName  - The name of the plan you want to add usage for.
 * @param {*} unitType  - the type of usage you want to add 
 * @param {*} units - integer containing the quantity of usage
 * @param {*} usageDateEpoch epoch timestamp for the date you want to add usage information for
 */
function addUsageToAccountSubscription(externalKey, subscriptionPlanName, unitType, units, usageDateEpoch) {

    AccountAPI.getByExternalKey(externalKey, "", (errAccountAPI, account) => {
        if (errAccountAPI) console.log("Account.get Err: ", errAccountAPI);
        console.log("account:\n", account);

        AccountAPI.getBundles(account.accountId, "", (getBundlesErr, bundles) => {
            if (getBundlesErr) console.log("Account.getBundles Err", getBundlesErr);
            // console.log("AccountAPI.getBundles\n": JSON.stringify(bundles, null, 2));

            if (bundles.length > 0) {
                bundle = bundles.find(bundle => bundle.subscriptions.find(subscription => (subscription.planName === subscriptionPlanName && subscription.state === "ACTIVE")));
                const subscription = bundle.subscriptions.find(subscription => (subscription.planName === subscriptionPlanName ));

                console.log("Get Subscription: \n", subscription);

                const usageRecord = {
                    subscriptionId: subscription.subscriptionId,
                    unitUsageRecords: [
                        {
                            unitType: unitType,
                            usageRecords: [
                                {
                                    recordDate: formatDateYYMMDD(usageDateEpoch),
                                    amount: units,
                                }
                            ]
                        }
                    ],
                };
                UsageAPI.create(usageRecord, "Sam", "testing", "", {},
                    (usageCreateErr, response) => {
                        if (usageCreateErr) console.log("Usage.create Err: ", usageCreateErr);
                        console.log("Create Usage response: \n", response);


                        UsageAPI.getByDate(subscription.subscriptionId,
                            formatDateYYMMDD(usageDateEpoch ),
                            formatDateYYMMDD(usageDateEpoch + dayInMillis), {},
                            (getByDateErr, usages) => {
                                if (getByDateErr) console.log("Usage.get Err:", getByDateErr);
                                console.log("Get usages: \n", usages);
                            }
                        );
                    }
                );

            }
        });
    });
}

function getUsages(externalKey, subscriptionPlanName, usageDateFromEpoch, usageDateToEpoch) {

    AccountAPI.getByExternalKey(externalKey, "", (errAccountAPI, account) => {
        if (errAccountAPI) console.log("Account.get Err: ", errAccountAPI);
        console.log("account:\n", account);

        AccountAPI.getBundles(account.accountId, "", (getBundlesErr, bundles) => {
            if (getBundlesErr) console.log("Account.getBundles Err", getBundlesErr);
             console.log("AccountAPI.getBundles\n", JSON.stringify(bundles, null, 2));

            if (bundles.length > 0) {
                bundle = bundles.find(bundle => bundle.subscriptions.find(subscription => subscription.planName === subscriptionPlanName));
                const subscription = bundle.subscriptions.find(subscription => subscription.planName === subscriptionPlanName);

                console.log("Get Subscription: \n", subscription);


                UsageAPI.getByDate(subscription.subscriptionId,
                    formatDateYYMMDD(usageDateFromEpoch ),
                    formatDateYYMMDD(usageDateToEpoch + dayInMillis), {},
                    (getByDateErr, usages) => {
                        if (getByDateErr) console.log("Usage.get Err:", getByDateErr);
                        console.log("Get usages: \n", usages);
                    }
                );

            }
        });
    });
}




function formatDateYYMMDD(timeInMilliseconds) {
    const d = new Date(timeInMilliseconds);
    const returnDate = d.getUTCFullYear() + "-" + (d.getUTCMonth() + 1) + '-' + d.getUTCDate();
    console.log(returnDate);
    return returnDate;
}
