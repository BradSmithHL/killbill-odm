This repository has information about the KillBill implementation for ODM

This document describes the structure of the repository and information about how to connect to and debug KillBill


It has 4 sub sections each with a README.txt:

Catalogue - Has an ODM KillBill catalogue and some information about how test and upload to killbill.
EmailPlugin - Has information about how to load and configure the EmailPlugin for KillBill, including email templates.
POC - Has some 'Proof Of Concept' code that uploads usage information to KillBill and downloads unpaid invoices.
StripePlugin - Has information about how to load and configure the stripe pluging and setup for a customer.


===========================================================
KAUI (Kill Bill Admin User Interface) - login

https://killbill.hlcloud.com.au/users/sign_in
u: hladmin
p: Dav3Luvskayn3

===========================================================
KillBill Server  - SSH, Docker, Logfiles, MYSQL 

SSH Connection
ssh -i "HL-Key.pem" ec2-user@ec2-54-206-62-23.ap-southeast-2.compute.amazonaws.com

(Get HL-Key.pem from AWS)

KillBill Docker container

To view the docker containers:
> sudo docker ps

CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                                                                      NAMES
fa37c1506292        killbill/killbill:0.20.0   "bash -c '$KPM_INSTA…"   6 days ago          Up 6 days           0.0.0.0:8000->8000/tcp, 0.0.0.0:8080->8080/tcp, 0.0.0.0:12345->12345/tcp   ec2user_killbill_1
c15e5ad64a26        killbill/kaui:1.0.4        "/etc/init.d/kaui.sh…"   2 months ago        Up 6 days           0.0.0.0:9090->8080/tcp                                                     ec2user_kaui_1
cf3095b7fa92        killbill/mariadb:0.20      "docker-entrypoint.s…"   2 months ago        Up 6 days           3306/tcp                                                                   ec2user_db_1


To connect to killbill docker (opens a bash shell ):
- open a ssh connection then:
> sudo docker exec -it fa37c1506292 bash


Killbill Logfiles
- Once logged into killbill docker, log files are in ./logs directory
    - killbill.out - contains the current logfile for killbill, as requests are made killbill log messages will appear in this directory.
    - catalina.out - contains logfiles from the tomcat servlet engine (think java webserver basically) which runs killbill.



KillBill MYSQL Database

To connect to mysql, connect to killbill docerk
> mysql -u root -h db killbill -p
password: killbill

to view all tables:
show tables;


