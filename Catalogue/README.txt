OVERVIEW

The Catalog is at the heart of the billing system. It is a data model that captures the core configuration of the billing system

This section has Killbill catalogues designed for ODM and describes how to upload and test them.

===========================================================

SETUP REQUIRMENTS for Catalogue testing tool.

Catalogue Tool needed following version of Java:

openjdk version "1.8.0_191"
OpenJDK Runtime Environment (build 1.8.0_191-8u191-b12-0ubuntu0.18.04.1-b12)
OpenJDK 64-Bit Server VM (build 25.191-b12, mixed mode)

===========================================================

Test a Catalogue (commandline)

java -jar killbill-catalog-*-load-tool.jar ./catalogue.xml

===========================================================

Uploading Catalogue (with KAUI to Killbill)

odm-testing-catalogue.xml - contains a daily and monthly usage based catalogue for odm

odm-monthly-catalogue.xml - contains a monthly usage based catalogue for odm

Log into Kaui and upload catalogue against tennant H&LAustralia ODM


===============================================================

Assigning a subscription to a customer

Log in to Killbill
Select the customer you want to assign the subscription to.
Navigate to the Subscriptions Tab
Click the + button next to 'Subscriptions'
Add  a key if you want
Select 'postpaid-odm-daily' 
Select 'Specify a date' 
Click on the first of day of the current month - this forces billing to be on the first day of the month.